import api from './Api.js'

const getAll = (filters) => {
    const params = {
        showViral: filters.showViral
    };

    return api.get(`/gallery/${filters.section}/${filters.sort}/${filters.window}/${filters.page}`, { params });
}

export default { getAll };