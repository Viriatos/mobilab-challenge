import axios from "axios";

let api = axios.create({
    baseURL: "https://api.imgur.com/3",
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Client-ID 78b9aae941275d6'
    }
});

export default api;