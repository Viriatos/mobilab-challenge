# How to Install and Run the project

All the dependecies must be installed first. To do so, run the following command:

* `npm install`

After installing the dependecies, the project is ready to run.

* `npm start`

The server will run at `http://localhost:8080/` .  
To start browsing galleries you can follow the instructions at `mobilab-client` folder.

### Stack used

* express
* axios
