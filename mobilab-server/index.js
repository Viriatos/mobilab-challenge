import Express from 'express';
import cors from 'cors';
import GalleriesAPI from './Services/Api/Galleries.js';

const app = Express();
const port = 8080;

app.use(cors());

app.get('/gallery/:section?/:sort?/:window?/:page?', (req, res) => {

    const filters = {
        section: req.params.section || 'hot',
        sort: req.params.sort || 'viral',
        windows: req.params.window || 'day',
        page: req.params.page || 0,
        showViral: req.query.showViral || true
    }

    GalleriesAPI.getAll(filters)
        .then(response => {
            const galleries = response.data.data;
            res.json(galleries);
        })
        .catch((error) => {
            res.json(error);
        });
});



app.listen(port, () => console.log('Access http://localhost.com:' + port + '/gallery'));