# Mobilab Challenge #

Hello and welcome to the Mobilab Challenge.  
The objective of such challenge is to use the `Imgur API` to fetch galleries and display them on the browser.

To achieve this, two folders were created.  
One includes all the code for the client, the other works as a proxy server.

* `mobilab-client` - React + Redux
* `mobilab-server` - Express


For more instructions on how to run the project, check the ReadMe file inside each folder.

**Thank you,  
Miguel Antunes**