# How to Install and Run the project

First of all, we must install all the dependecies. To do so, run the following command:

* `npm install`

After installing the dependecies, you're ready to run the project.

* `npm start`

Before browsing galleries at `http://localhost:3000/` , you must run the project inside the `mobilab-server` folder.  
The server will fetch the galleries for you. Instructions on its folder.

### Stack used

* react
* redux
* tailwindcss
* axios
* redux-thunk
