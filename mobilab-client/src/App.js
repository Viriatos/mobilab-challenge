import './tailwind.css';
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Header from './Components/ui-elements/Header';
import Footer from './Components/ui-elements/Footer';
import Views from './Views';

function App() {
  return (
    <div>
      <Router>
        <Header />

        <Switch>
          <Route exact path="/" component={Views.Galleries} />
          <Route path="/gallery/:id" component={Views.Gallery} />
        </Switch>

        <Footer />
      </Router>
    </div>
  );
}

export default App;