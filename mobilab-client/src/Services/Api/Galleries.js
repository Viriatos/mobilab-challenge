import api from './Api';

const getAll = (filters) => {
  const params = {
    showViral: filters.showViral
  }
  return api.get(`/gallery/${filters.section}/${filters.sort}/${filters.window}/${filters.page}`, { params });
}

const GalleriesAPI = {
  getAll
}

export default GalleriesAPI;