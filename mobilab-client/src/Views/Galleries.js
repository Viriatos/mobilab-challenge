import React from 'react';
import GalleryGrid from '../Components/ui-elements/Gallery/Grid'

function Galleries() {

    return (
        <GalleryGrid />
    );
}

export default Galleries;