import Galleries from './Galleries';
import Gallery from './Gallery';

const views = {
    Galleries,
    Gallery,
};

export default views;

