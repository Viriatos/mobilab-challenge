import React, { useEffect, useState } from 'react';
import GalleryDetails from '../Components/ui-elements/Gallery/Details'
import { useLocation, useHistory } from 'react-router-dom';

function Gallery() {
    const location = useLocation();
    const history = useHistory();

    const [gallery, setGallery] = useState({
        loading: true,
        data: null
    });

    useEffect(() => {
        const shouldRedirect = () => {
            if (typeof (location.gallery) == 'undefined' || location.gallery == null) {
                history.push("/");
            }
        }

        shouldRedirect();
        setGallery({
            loading: false,
            data: location.gallery
        });
    }, [location, history]);

    return (
        <>
            {
                !gallery.loading &&
                < GalleryDetails
                    galleryId={gallery.data.id}
                    cover={gallery.data.cover}
                    title={gallery.data.title}
                    description={gallery.data.description}
                    ups={gallery.data.ups}
                    downs={gallery.data.downs}
                    score={gallery.data.score}
                />
            }
        </>
    );
}

export default Gallery;