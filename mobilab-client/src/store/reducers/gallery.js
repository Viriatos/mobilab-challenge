import { reducerFactory } from './utils/factory/reducerFactory';
import GALLERY_ACTION_TYPES from "../actions/gallery/galleryTypes";

const initialState = {
    list: [],
    filters: {
        section: 'hot',
        sort: 'viral',
        window: 'day',
        showViral: 'true',
        page: 0,
    }
};

const freshGalleries = (state, payload) => {
    return {
        ...state,
        list: [],
    };
}

const getGalleries = (state, payload) => {
    return {
        ...state,
        list: state.list.concat(payload.galleries),
    };
}

const changeFilters = (state, payload) => {
    return {
        ...state,
        filters: payload.filters,
        list: []
    };
}

const nextPage = (state, payload) => {
    return {
        ...state,
        filters: payload.filters
    };
}

const handlers = {}
handlers[GALLERY_ACTION_TYPES.FETCH_GALLERIES_REQUEST] = freshGalleries;
handlers[GALLERY_ACTION_TYPES.FETCH_GALLERIES_SUCCESS] = getGalleries;
handlers[GALLERY_ACTION_TYPES.CHANGE_FILTERS] = changeFilters;
handlers[GALLERY_ACTION_TYPES.NEXT_PAGE] = nextPage;

export default reducerFactory(initialState, handlers);

