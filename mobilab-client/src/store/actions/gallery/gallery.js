import GALLERY_ACTION_TYPES from './galleryTypes';
import GalleriesAPI from '../../../Services/Api/Galleries';


function getFromApi(
    dispatch,
    filters = {
        section: 'top',
        sort: 'viral',
        window: 'day',
        page: 0,
        showViral: 'true'
    }) {
    GalleriesAPI.getAll(filters)
        .then(response => {
            const galleries = response.data;
            dispatch(fetchGalleriesSuccess(galleries));
        })
        .catch((error) => {
            console.log(error);
            dispatch(fetchGalleriesFailure(error));
        });
}

const fetchGalleriesRequest = () => {
    return {
        type: GALLERY_ACTION_TYPES.FETCH_GALLERIES_REQUEST
    };
}

const fetchGalleriesSuccess = (galleries) => {
    return {
        type: GALLERY_ACTION_TYPES.FETCH_GALLERIES_SUCCESS,
        payload: { galleries }
    };
}

const fetchGalleriesFailure = (error) => {
    return {
        type: GALLERY_ACTION_TYPES.FETCH_GALLERIES_FAILURE,
        payload: error
    };
}

const changeFilter = (filters) => {
    return (dispatch) => {
        dispatch({
            type: GALLERY_ACTION_TYPES.CHANGE_FILTERS,
            payload: { filters }
        });
        getFromApi(dispatch, filters);
    }
}

const nextPage = (filters) => {
    return (dispatch) => {
        dispatch({
            type: GALLERY_ACTION_TYPES.NEXT_PAGE,
            payload: { filters }
        });
        getFromApi(dispatch, filters);
    }
}

const fetchGalleries = (filters) => {
    return (dispatch) => {
        dispatch(fetchGalleriesRequest());
        getFromApi(dispatch, filters);
    }
};


const GalleryActions = {
    fetchGalleriesRequest,
    fetchGalleriesSuccess,
    fetchGalleriesFailure,
    fetchGalleries,
    changeFilter,
    nextPage
};

export default GalleryActions;