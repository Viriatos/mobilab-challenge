import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import GalleryActions from '../../../store/actions/gallery/gallery';

function Header() {
    const dispatch = useDispatch();
    const galleryFilters = useSelector((state) => state.gallery.filters);

    const changeFilter = (value, type) => {
        const filters = {
            ...galleryFilters,
            [type]: value,
            page: 0
        };
        dispatch(GalleryActions.changeFilter(filters));
    }


    const sectionHtml = () => {
        return (
            <label>
                section
                <select className="ml-2 mr-2 border px-1 py-1" onChange={(e) => changeFilter(e.target.value, 'section')}>
                    <option value="hot">Hot</option>
                    <option value="top">Top</option>
                    <option value="user">User</option>
                </select>
            </label>
        );
    }

    const sortHtml = () => {
        return (
            <label>
                sort
                <select className="ml-2 mr-2 border px-1 py-1" onChange={(e) => changeFilter(e.target.value, 'sort')}>
                    <option value="viral">Viral</option>
                    <option value="top">Top</option>
                    <option value="time">Time</option>
                    <option value="rising">Rising</option>
                </select>
            </label>
        );
    }

    const windowHtml = () => {
        return (
            <label>
                window
                <select className="ml-2 mr-2 border px-1 py-1" onChange={(e) => changeFilter(e.target.value, 'window')}>
                    <option value="day">Day</option>
                    <option value="week">Week</option>
                    <option value="month">Month</option>
                    <option value="year">Year</option>
                    <option value="all">All</option>
                </select>
            </label>
        );
    }

    const showViralHtml = () => {
        return (
            <label>
                show viral
                <select className="ml-2 mr-2 border px-1 py-1" onChange={(e) => changeFilter(e.target.value, 'showViral')}>
                    <option value="true">true</option>
                    <option value="false">false</option>
                </select>
            </label>
        );
    }

    return (
        <header className="border-b font-bold p-3 md:flex justify-between items-center">
            <Link to="/">
                <span className="font-bold">
                    Mobilab - Imgur Galleries
                </span>
            </Link>

            <div className="flex justify-between items-center">
                {sectionHtml()}
                {galleryFilters.section === 'user' ? sortHtml() : null}
                {galleryFilters.section === 'top' ? windowHtml() : null}
                {showViralHtml()}
            </div>

        </header >
    );
}

export default Header;