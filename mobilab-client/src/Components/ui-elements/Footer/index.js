import React from 'react';

function Footer() {
    return (
        <footer className="font-bold fixed bg-gray-200 text-right text-xs p-3 bottom-0 w-full">
            Miguel Antunes - Mobilab Challenge 2020
        </footer>
    );
}

export default Footer;