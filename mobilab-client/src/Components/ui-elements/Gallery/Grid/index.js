import React, { useEffect } from 'react';
import GalleryThumbnail from './Thumbnail';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import GalleryActions from '../../../../store/actions/gallery/gallery';

function Grid() {
    const dispatch = useDispatch();
    const galleryState = useSelector((state) => state.gallery);
    const galleries = galleryState.list;
    const galleryFilters = galleryState.filters;

    const nextPage = (value, type) => {
        const filters = {
            ...galleryFilters,
            [type]: value
        };
        dispatch(GalleryActions.nextPage(filters));
    }

    useEffect((galleryFilters) => {
        dispatch(GalleryActions.fetchGalleries(galleryFilters));
    }, [dispatch]);

    const loadingHtml = () => {
        return (
            <div className="text-center">
                <svg className="animate-spin m-auto h-10 w-10 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
                    <circle className="opacity-100" cx="12" cy="12" r="10" stroke="red" strokeWidth="0"></circle>
                    <path className="opacity-50" fill="red" d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
                </svg>
            </div>
        );
    }

    const loadMoreHtml = () => {
        return (
            <div className="text-center">
                <button onClick={(e) => nextPage(galleryFilters.page + 1, 'page')} className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-red-700 focus:outline-none">
                    Load More
                </button>
            </div>
        );
    }

    const galleryThumbnailHtml = (gallery, index) => {
        return (
            <GalleryThumbnail
                key={index}
                galleryId={gallery.id}
                cover={gallery.cover}
                title={gallery.title}
                description={gallery.description}
                ups={gallery.ups}
                downs={gallery.downs}
                score={gallery.score}
            />
        );
    }

    return (
        <div className="container mt-5 mb-20 mx-auto px-4 md:px-12">
            {galleries.length <= 0 ? loadingHtml() : null}
            <div className="flex flex-wrap -mx-1 lg:-mx-4">
                {
                    galleries.length > 0 && galleries.map(
                        (gallery, index) => {
                            return gallery.cover != null ? galleryThumbnailHtml(gallery, index) : null
                        }
                    )
                }
            </div>
            {galleries.length > 0 ? loadMoreHtml() : null}
        </div>
    );
}

export default Grid;