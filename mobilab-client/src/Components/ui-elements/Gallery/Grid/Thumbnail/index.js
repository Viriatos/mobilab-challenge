import React from 'react';
import { Link } from 'react-router-dom';

function Thumbnail(props) {
    const BASE_COVER_URL = 'http://i.imgur.com/';

    function getCover(cover) {
        return `${BASE_COVER_URL}/${cover}.gif`;
    }

    function getImageUrl() {
        return '/gallery/' + props.galleryId;
    }

    return (
        <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/3">
            <Link
                to={{
                    pathname: getImageUrl(),
                    gallery: {
                        id: props.galleryId,
                        cover: getCover(props.cover),
                        title: props.title,
                        description: props.description,
                        ups: props.ups,
                        downs: props.downs,
                        score: props.score
                    }
                }}>
                <figure className="overflow-hidden rounded-lg shadow-lg">
                    {<img className="object-cover h-64 w-full" alt={props.galleryId} src={getCover(props.cover)} />}
                    <figcaption className="flex items-center justify-between leading-tight p-2 md:p-4">
                        <h1 className="text-lg w-full">
                            <span className="text-center w-full text-black">
                                {props.description}
                            </span>
                        </h1>
                    </figcaption>
                </figure>
            </Link>
        </div>
    );
}

export default Thumbnail;