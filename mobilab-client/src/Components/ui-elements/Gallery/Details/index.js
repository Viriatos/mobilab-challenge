import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faArrowUp, faStar } from '@fortawesome/free-solid-svg-icons';

function Details(props) {

    return (
        <>
            <div className="absolute m-2">
                <Link
                    to='/'>
                    <button className="py-2 px-4 bg-red-500 text-white font-semibold rounded-lg shadow-md hover:bg-red-700 focus:outline-none">
                        All Galleries
                    </button>
                </Link>
            </div>
            <div className="flex items-center justify-center">
                <div className="my-1 px-1 w-full md:w-1/2 lg:my-4 lg:px-4 lg:w-1/2 pb-20">
                    <figure className="overflow-hidden rounded-lg shadow-lg">

                        {<img className="object-cover w-full" alt={props.galleryId} src={props.cover} />}
                        <figcaption className="">
                            <h1 className="pb-2 pt-2 pl-2 font-bold text-lg">{props.title}</h1>
                            <h1 className="pb-2 pt-2 pl-2 border-b text-lg">{props.description}</h1>

                            <div className="flex pb-2 items-center justify-between leading-tight p-2 md:p-4">
                                <span className="text-lg">{props.ups} <FontAwesomeIcon className="text-green-700" icon={faArrowUp} /></span>
                                <span className="text-lg">{props.downs} <FontAwesomeIcon className="text-red-700" icon={faArrowDown} /></span>
                                <span className="text-lg">{props.score} <FontAwesomeIcon className="text-yellow-700" icon={faStar} /></span>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>

        </>
    );
}

export default Details;